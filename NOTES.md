# Curl commands

Curl requests for basic apple developer console operations.

## Login

Login using e-mail and password and get auth cookie:

    curl --location --data "appIdKey=<apple_app_id_key>&appleId=<apple_id_email>&accountPassword=<apple_password>" "https://idmsa.apple.com/IDMSWebAuth/authenticate" --cookie-jar ./.authenticate_cookie

## Get list of Pass Type IDs

Get list of pass IDs. There is ownerDisplayId in list for each ID. It is used for getting detailes, creating certificate, etc.

    curl 'https://developer.apple.com/services-account/QH65B2/account/ios/identifiers/listPassTypeIds.action?content-type=text/x-url-arguments&accept=application/json&requestId=e94faaca-148a-11e8-b642-0ed5f89f718b&userLocale=en_US&teamId=<apple_team_id>' --cookie ./.authenticate_cookie -H 'Content-Type: application/x-www-form-urlencoded' -H 'Accept: application/json, text/javascript, */*; q=0.01' --data 'search=&nd=1518944038532&pageSize=500&pageNumber=1&sidx=name&sort=name%253dasc' --compressed

## Get Pass Type ID detail

    curl 'https://developer.apple.com/services-account/QH65B2/account/ios/certificate/listCertificatesForOwner.action?content-type=text/x-url-arguments&accept=application/json&requestId=96bf9fdd-ea67-4ef6-y552-4103253a635e&userLocale=en_US&teamId=<apple_team_id>' --cookie ./.authenticate_cookie -H 'Content-Type: application/x-www-form-urlencoded' --data 'types=Y3B2F3TYSI&ownerDisplayId=NPTR539N95' --compressed

## Create new Pass Type ID

    curl -i 'https://developer.apple.com/services-account/QH65B2/account/ios/identifiers/validatePassTypeId.action?content-type=text/x-url-arguments&accept=application/json&requestId=1c6a3441-5a1e-4b82-y48f-e61e8c7d875b&userLocale=en_US&teamId=<apple_team_id>' --cookie ./.authenticate_cookie  -H 'Content-Type: application/x-www-form-urlencoded' --data 'name=Storyous+Customer+14&identifier=pass.ys.cz.storyous.customer-14' --compressed

Use csrf and csrf_ts from request:

    curl 'https://developer.apple.com/services-account/QH65B2/account/ios/identifiers/addPassTypeId.action?content-type=text/x-url-arguments&accept=application/json&requestId=ead5b4a9-b848-4187-ycd2-616a31da2aeb&userLocale=en_US&teamId=<apple_team_id>' --cookie ./.authenticate_cookie -H 'csrf: c28220a69ead4700dab9ab273f272c1e746ce361bc6058ff48c596a506c48f86' -H 'X-Requested-With: XMLHttpRequest' -H 'Content-Type: application/x-www-form-urlencoded' -H 'csrf_ts: 1518958720329' --data 'name=Storyous+Customer+14&identifier=pass.ys.cz.storyous.customer-14' --compressed

## Create certificate using CSR

Use csrf and csrf_ts:

    curl 'https://developer.apple.com/services-account/QH65B2/account/ios/certificate/submitCertificateRequest.action?content-type=text/x-url-arguments&accept=application/json&requestId=fb07359d-5d64-4d90-yb99-f41bfab4cd91&userLocale=en_US&teamId=<apple_team_id>' --cookie ./.authenticate_cookie -H 'csrf: e9cb77107be8bf703edc8ae1bebe929c19e668a4de79c29ad698ecbc5a538c13' -H 'X-Requested-With: XMLHttpRequest' -H 'csrf_ts: 1518973737942' --data 'type=Y3B2F3TYSI&csrContent=-----BEGIN+CERTIFICATE+REQUEST-----%0AMIICRTCCAS0CAQAwADCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAM%2B3%0A%2FwWr6xyIqJ8ZAN7dPzIWDnaR3z8hu8o4METVT2IQ65DhPVtpbBaSCWZxzbNoAA%2Bb%0A0dqN%2BqfqPzZjJmiIs%2FeiwVEAFjYQ5q7ZCdrAj%2FWSnchwlBOY8VDnR0mqo9%2BqOaVE%0AKAK0LqVzmIpEEthkSXre7VzgVAt65K9J27ODiLSvkox523X%2BLRx%2FwrSU2Pf08saw%0AthqiJTEp0Fa%2Bd706qL0MV2j3Us1dtbcVekl7q0VfVHIkaEMfG63KxnB7c9fdjG0Z%0A98lnv8nSwO3W5Il8POHUIp0szGRAjLJsnIPoGYelIBhNSkJVBMR1bVYzoeC3Jubs%0AG%2FQ%2BUIWSRXymRnMmBvMCAwEAAaAAMA0GCSqGSIb3DQEBCwUAA4IBAQCv3e7H%2BIsL%0AA26x6betn1HwwVSR2%2FiKu%2B4EbUk%2B4i5zk%2FrhOFBtdSorsuleI8SnhsUt5z3Rnf3V%0AGszpgiqmSpNk33keHouMIIZwrYCI6CYrlKXZhiLMhr5Y9pT%2BpG%2BlareoY62jMSh4%0AVr%2Bwi1dtkmWpFlU4NHSUApBnp96bOduYYLQkX8PIrDZODDWoXFGU6I0QDFPAvtlq%0A8N%2B2c6kexWZ7ha2EBPsS0js7daJkt1oPhpVn5sDsI89j%2FzMpSsMyRmGD5L9owb%2Be%0ABfayWyOW%2BsbvlBmnJOjpTLYk5Tu9m9wHrP%2BQ6DP3cjRePPOd0sEOAgnEDPfLN9op%0Av1oEdGZgQ4dW%0A-----END+CERTIFICATE+REQUEST-----%0A&passTypeId=NPTR539N95&specialIdentifierDisplayId=NPTR539N95' --compressed

## Get certificate detail

    curl 'https://developer.apple.com/services-account/QH65B2/account/ios/certificate/getCertificate.action?content-type=text/x-url-arguments&accept=application/json&requestId=7184a453-f1c9-4b90-yfaf-036d4aebc544&userLocale=en_US&teamId=<apple_team_id>' --cookie ./.authenticate_cookie -H 'X-Requested-With: XMLHttpRequest' -H 'Content-Type: application/x-www-form-urlencoded' --data 'certificateId=3URB9TTF3P&type=Y3B2F3TYSI' --compressed

## Download certicate

    curl https://developer.apple.com/services-account/QH65B2/account/ios/certificate/downloadCertificateContent.action?content-type=text/x-url-arguments&accept=application/json&requestId=72f87d1c-510f-4ca2-y886-8d73f700dcd8&userLocale=en_US&teamId=<apple_team_id>&certificateId=3URB9TTF3P&type=Y3B2F3TYSI --cookie ./.authenticate_cookie