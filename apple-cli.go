package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/alecthomas/template"
	"github.com/subosito/gotenv"
	"github.com/urfave/cli"

	"apple-cli/lib/appleclient"
)

var client *appleclient.AppleClient

// Command line arguments - using Destination because arguments are not available in context in subcommands:(
var appleIDKey, appleID, applePassword, teamID string
var format, formatTmpl string
var indentJSON, debug bool

// OutputTemplate for text output definition
type OutputTemplate struct {
	template string
}

var templates = map[string]OutputTemplate{
	"cert-list": OutputTemplate{
		`{{range $i, $el := .CertRequests}}{{$el.Name}} {{$el.CertificateID}} {{$el.Certificate.ExpirationDate}}{{"\n"}}{{end}}`,
	},
	"cert-get": OutputTemplate{
		`{{. | printf "%+v"}}`,
	},
	"cert-create": OutputTemplate{
		`{{.CertRequest.Certificate.CertificateID}}{{"\n"}}`,
	},
	"pass-list": OutputTemplate{
		`{{range $i, $el := .PassTypeIDList}}{{$el.Name}} {{$el.Identifier}} {{$el.PassTypeID}}{{"\n"}}{{end}}`,
	},
	"pass-get": OutputTemplate{
		`{{. | printf "%+v"}}`,
	},
	"pass-add": OutputTemplate{
		`{{.PassTypeID.PassTypeID}}{{"\n"}}`,
	},
}

// Main init
func init() {
	// Read configuration from .env if exists
	gotenv.Load()
}

func main() {
	app := cli.NewApp()
	app.Name = "apple-cli"
	app.Usage = "Command line client for Apple Developer Console"
	app.Version = "1.0.0"

	// Global flags
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "apple_key",
			Value:       "",
			Usage:       "Apple API KEY",
			EnvVar:      "APPLE_ID_KEY",
			Destination: &appleIDKey,
		},
		cli.StringFlag{
			Name:        "apple_id",
			Value:       "",
			Usage:       "Apple ID",
			EnvVar:      "APPLE_ID",
			Destination: &appleID,
		},
		cli.StringFlag{
			Name:        "apple_password",
			Value:       "",
			Usage:       "Apple password",
			EnvVar:      "APPLE_PASSWORD",
			Destination: &applePassword,
		},
		cli.StringFlag{
			Name:        "team_id",
			Value:       "",
			Usage:       "Apple team ID",
			EnvVar:      "TEAM_ID",
			Destination: &teamID,
		},
		cli.StringFlag{
			Name:        "format",
			Value:       "text",
			Usage:       "Print output format - text or json",
			EnvVar:      "FORMAT",
			Destination: &format,
		},
		cli.StringFlag{
			Name:        "format_tmpl",
			Value:       "",
			Usage:       "Default template for text output",
			EnvVar:      "TEMPLATE",
			Destination: &formatTmpl,
		},
		cli.BoolTFlag{
			Name:        "indent",
			Usage:       "Indent JSON output format - default is true for nice output",
			Destination: &indentJSON,
		},
		cli.BoolFlag{
			Name:        "debug",
			Usage:       "Print debug information, eg. request headers, etc.",
			Destination: &debug,
		},
	}

	// Client command line definition
	app.Commands = []cli.Command{
		{
			Name:  "cert",
			Usage: "certificate tasks",
			Subcommands: []cli.Command{
				{
					Name:    "list",
					Aliases: []string{"ls"},
					Usage:   "returns list of certificates",
					Action:  listCertificates,
				},
				{
					Name:   "get",
					Usage:  "returns certificate detail with certificate ID",
					Action: getCertificateDetail,
				},
				{
					Name:   "download",
					Usage:  "downloads certificate to pass.cer or to --outfile",
					Action: downloadCertificate,
					Flags: []cli.Flag{
						cli.StringFlag{
							Name:  "outfile",
							Value: "pass.cer",
							Usage: "certificate output file",
						},
					},
				},
				{
					Name:    "create",
					Aliases: []string{"add"},
					Usage:   "creates certificate for Pass Type ID and CSR",
					Action:  createCertificate,
				},
			},
		},
		{
			Name:  "pass",
			Usage: "Pass Type ID tasks",
			Subcommands: []cli.Command{
				{
					Name:    "list",
					Aliases: []string{"ls"},
					Usage:   "returns list of Pass Type IDs; use --search for searching",
					Action:  listPassTypeIDs,
					Flags: []cli.Flag{
						cli.StringFlag{
							Name:  "search",
							Value: "",
							Usage: "search parameter, eg. for Pass Type ID with name use 'name=pass.ys.cz.example&identifier=pass.ys.cz.example'",
						},
					},
				},
				{
					Name:   "get",
					Usage:  "returns Pass Type ID detail",
					Action: getPassTypeIDDetail,
				},
				{
					Name:    "validate",
					Aliases: []string{"val"},
					Usage:   "validates Pass Type ID",
					Action:  validatePassTypeID,
				},
				{
					Name:   "add",
					Usage:  "adds new Pass Type IDs",
					Action: addPassTypeID,
				},
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		checkError(err)
	}
}

// Common client function

func initClient() {
	// Init Apple client
	config := appleclient.Config{
		AppIDKey:        appleIDKey,
		AppleID:         appleID,
		AccountPassword: applePassword,
		TeamID:          teamID,
		PrintDebug:      debug,
	}
	client = appleclient.NewAppleClient(config)
	err := client.Login()
	checkError(err)
}

func checkError(err error) {
	if err != nil {
		fmt.Println("Fatal error: ", err.Error())
		os.Exit(1)
	}
}

func printResult(result interface{}, tmpl string) {
	if format == "json" {
		printJSON(result)
	} else {
		printText(result, tmpl)
	}
}

func printText(data interface{}, tmpl string) {
	var useTmpl = tmpl
	// Use template from command argument if defined
	if formatTmpl != "" {
		useTmpl = formatTmpl
	}
	// Default template
	if useTmpl == "" {
		useTmpl = "{{.}}"
	}
	t := template.Must(template.New("tmpl").Parse(useTmpl))
	err := t.Execute(os.Stdout, data)
	if err != nil {
		fmt.Println("Executing template:", err)
	}
}

func printJSON(data interface{}) {
	var jsonOutput []byte
	if indentJSON {
		jsonOutput, _ = json.MarshalIndent(data, "", "\t")
	} else {
		jsonOutput, _ = json.Marshal(data)
	}
	fmt.Println(string(jsonOutput))
}

// Certificate commands

func listCertificates(c *cli.Context) {
	initClient()
	result, err := client.ListCertificates()
	checkError(err)

	printResult(result, templates["cert-list"].template)
}

func getCertificateDetail(c *cli.Context) {
	certificateID := c.Args().First()
	if certificateID == "" {
		checkError(errors.New("Specify certificate ID"))
	}
	initClient()
	result, err := client.GetCertificateDetail(certificateID)
	checkError(err)
	printResult(result, templates["cert-get"].template)
}

func downloadCertificate(c *cli.Context) {
	certificateID := c.Args().First()
	if certificateID == "" {
		checkError(errors.New("Specify certificate ID"))
	}
	initClient()
	cert, err := client.DownloadCertificate(certificateID)
	checkError(err)
	outFile := c.String("outfile")
	ioutil.WriteFile(outFile, cert, 0644)
}

func createCertificate(c *cli.Context) {
	if c.NArg() != 2 {
		checkError(errors.New("Specify Pass Type ID and file with CSR"))
	}
	passTypeID := c.Args().First()
	csrFilePath := c.Args().Get(1)
	initClient()

	// Get CSRF token from permission request
	err := client.CheckPermissions()
	checkError(err)

	// Create certificate
	csr, err := ioutil.ReadFile(csrFilePath)
	checkError(err)
	createCertificateResult, err := client.CreateCertificate(passTypeID, csr)
	checkError(err)
	printResult(createCertificateResult, templates["cert-create"].template)
}

// Pass type ID commands

func listPassTypeIDs(c *cli.Context) {
	initClient()
	result, err := client.ListPassTypeIDs(c.String("search"))
	checkError(err)
	printResult(result, templates["pass-list"].template)
}

func getPassTypeIDDetail(c *cli.Context) {
	passID := c.Args().First()
	if passID == "" {
		checkError(errors.New("Specify Pass Type ID"))
	}
	initClient()
	result, err := client.GetPassTypeIDDetail(passID)
	checkError(err)
	printResult(result, templates["pass-get"].template)
}

func validatePassTypeID(c *cli.Context) {
	if c.NArg() != 2 {
		checkError(errors.New("Specify Pass name and ID"))
	}
	initClient()
	passName := c.Args().First()
	passID := c.Args().Get(1)
	result, err := client.ValidatePassTypeID(passName, passID)
	checkError(err)
	printResult(result, "")
}

func addPassTypeID(c *cli.Context) {
	if c.NArg() != 2 {
		checkError(errors.New("Specify Pass name and ID"))
	}
	initClient()
	passName := c.Args().First()
	passID := c.Args().Get(1)
	_, err := client.ValidatePassTypeID(passName, passID)
	checkError(err)
	result, err := client.CreatePassTypeID(passName, passID)
	checkError(err)
	printResult(result, templates["pass-add"].template)
}
