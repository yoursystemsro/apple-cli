
# Apple Developer Console Client

This project contains source code for Apple Developer Console Client library and command line interface. 

# Motivation

If you develop anything for Apple you need to use [Apple Developer Console](https://developer.apple.com/). Console does not have any API (eg. REST). You have to use web interface. It is hard to integrate console operations into some other systems and applications.

Apple Developer Console Client has been developed to allow to call console operations from an application or from command line.

It does not implement all the console features. The following operations has been implemented:

- list of Pass Type ID
- get Pass Type ID detail
- add new Pass Type ID
- list of Certificates
- get Certificate detail
- create new Certificate
- download certificate

It is developed using [Golang](http://golang.org/).


# Usage

## Basic

    config := appleclient.Config{
		AppIDKey:        appleIDKey,
		AppleID:         appleID,
		AccountPassword: applePassword,
		TeamID:          teamID,
		PrintDebug:      debug,
	}
	client = appleclient.NewAppleClient(config)
	err := client.Login()
    if err != nil {
		fmt.Println("Fatal error: ", err.Error())
		os.Exit(1)
	}
    result, err := client.ListCertificates()
    fmt.Println("%q", result)

## Client configuration

Before you begin to use client you need to configure it.

Configuration in Golang struct:

    Config struct {
		AppIDKey        string
		AppleID         string
		AccountPassword string
		TeamID          string
		PrintDebug      bool
	}

For cli you can use arguments or enviroment variables:

    --apple_key value       Apple API KEY [$APPLE_ID_KEY]
    --apple_id value        Apple ID [$APPLE_ID]
    --apple_password value  Apple password [$APPLE_PASSWORD]
    --team_id value         Apple team ID [$TEAM_ID]

You can specify enviroment variables using `.env` also:

    APPLE_ID_KEY=""
    APPLE_ID=""
    APPLE_PASSWORD=""
    TEAM_ID=""

## Command line client

You can build command line client using:

    go build

It builds `apple-cli` binary.

Or use:

    go install

It installs binary into `$GOPATH/bin/apple-cli`.

### Usage

    NAME:
    apple-cli - Command line client for Apple Developer Console

    USAGE:
    apple-cli [global options] command [command options] [arguments...]

    VERSION:
    1.0.0

    COMMANDS:
        cert     certificate tasks
        pass     Pass Type ID tasks
        help, h  Shows a list of commands or help for one command

    GLOBAL OPTIONS:
    --apple_key value       Apple API KEY [$APPLE_ID_KEY]
    --apple_id value        Apple ID [$APPLE_ID]
    --apple_password value  Apple password [$APPLE_PASSWORD]
    --team_id value         Apple team ID [$TEAM_ID]
    --format value          Print output format - text or json (default: "text") [$FORMAT]
    --format_tmpl value     Default template for text output [$TEMPLATE]
    --indent                Indent JSON output format - default is true for nice output
    --debug                 Print debug information, eg. request headers, etc.
    --help, -h              show help
    --version, -v           print the version


### Pass Type ID commands

    NAME:
    apple-cli pass - Pass Type ID tasks

    USAGE:
    apple-cli pass command [command options] [arguments...]

    COMMANDS:
        list, ls       returns list of Pass Type IDs; use --search for searching
        get            returns Pass Type ID detail
        validate, val  validates Pass Type ID
        add            adds new Pass Type IDs

    OPTIONS:
    --help, -h  show help

### Certificate commands

    NAME:
    apple-cli pass - Pass Type ID tasks

    USAGE:
    apple-cli pass command [command options] [arguments...]

    COMMANDS:
        list, ls       returns list of Pass Type IDs; use --search for searching
        get            returns Pass Type ID detail
        validate, val  validates Pass Type ID
        add            adds new Pass Type IDs

    OPTIONS:
    --help, -h  show help
    
    ➜  apple-cli git:(master) ✗ go run apple-cli.go cert
    NAME:
    apple-cli cert - certificate tasks

    USAGE:
    apple-cli cert command [command options] [arguments...]

    COMMANDS:
        list, ls     returns list of certificates
        get          returns certificate detail with certificate ID
        download     downloads certificate to pass.cer or to --outfile
        create, add  creates certificate for Pass Type ID and CSR

    OPTIONS:
    --help, -h  show help

### Command output formats

#### Text

Default command output format is text. Text using predefined GO templates for formating each result. You can specify custom output format using global *--format_tmpl <go template>*. eg.

    --format_tmpl '{{range $i, $el := .CertRequests}}{{$el.Name}} {{$el.CertificateID}} {{$el.Certificate.ExpirationDate}}{{"\n"}}{{end}}'

You can find result structs in `lib/appleclient/types.go`.

#### JSON

Use *--format json* global parameter for output in JSON.

# Contribute

If you would like to contribute, please let us know. We are open for all contributors.

# Contact

Yoursystem, s.r.o. Jan Simunek <jan.simunek@ys.cz>

# Credits

Thanks for developers of:

- http://golang.org/
- https://mholt.github.io/curl-to-go/
- https://mholt.github.io/json-to-go/
- https://github.com/urfave/cli


# License

Copyright (c) 2018 Your system, s.r.o.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.



# Curl commands

You can find curl commands used for development in `NOTES.md`.