package appleclient

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

// ListPassTypeIDs - get list of pass type IDs
func (client *AppleClient) ListPassTypeIDs(search string) (*PassTypeIDListAPIResponse, error) {
	client.printDebug("Listing pass type ids")
	body := strings.NewReader(`teamId=` + client.Config.TeamID + `&search=` + url.QueryEscape(search) + `&nd=1518944038532&pageSize=1000&pageNumber=1&sidx=name&sort=name%253dasc`)
	req, err := http.NewRequest("POST", listPassTypeIDURL, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := client.HTTPClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	defer client.ParseCsrf(resp)

	if resp.StatusCode != 200 {
		return nil, NewErrAppleClient("Error getting list of pass type IDs")
	}

	// Parse JSON
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var s = new(PassTypeIDListAPIResponse)
	err = json.Unmarshal(respBody, &s)

	// Check ResultCode from JSON result
	if s.ResultCode != 0 {
		return s, NewErrAppleClient("Error getting list of pass type IDs")
	}

	return s, err
}

// GetPassTypeIDDetail - return Pass Type ID detail
func (client *AppleClient) GetPassTypeIDDetail(certificateID string) (*PassTypeIDDetailPIResponse, error) {
	client.printDebug("Getting Pass Type ID detail")
	body := strings.NewReader(`ownerDisplayId=` + certificateID + `&types=Y3B2F3TYSI`)
	req, err := http.NewRequest("POST", getPassTypeIDDetailURL+"&teamId="+client.Config.TeamID, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("X-Requested-With", "XMLHttpRequest")
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Csrf", client.Csrf)
	req.Header.Set("Csrf_ts", client.CsrfTs)

	resp, err := client.HTTPClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	defer client.ParseCsrf(resp)

	// Check HTTP response code
	if resp.StatusCode != 200 {
		return nil, NewErrAppleClient("Error getting Pass Type ID detail")
	}

	// Parse JSON
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var s = new(PassTypeIDDetailPIResponse)
	err = json.Unmarshal(respBody, &s)

	// Check ResultCode from JSON result
	if s.ResultCode != 0 {
		return s, NewErrAppleClient("Error getting Pass Type ID detail")
	}

	return s, err
}

// ValidatePassTypeID - Validate new Pass Type ID - use before AddPassTypeID
func (client *AppleClient) ValidatePassTypeID(name, id string) (*ValidatePassTypeIDAPIResponse, error) {
	client.printDebug("Validating pass type ID")
	body := strings.NewReader(`teamId=` + client.Config.TeamID + `&name=` + name + `&identifier=` + id)
	req, err := http.NewRequest("POST", validatePassTypeIDURL, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Csrf", client.Csrf)
	req.Header.Set("Csrf_ts", NewCsrtTs())

	resp, err := client.HTTPClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	defer client.ParseCsrf(resp)

	if resp.StatusCode != 200 {
		return nil, NewErrAppleClient("Error validation Pass Type IDs")
	}

	// Parse JSON
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var s = new(ValidatePassTypeIDAPIResponse)
	err = json.Unmarshal(respBody, &s)

	// Check ResultCode from JSON result
	if s.ResultCode != 0 {
		return s, NewErrAppleClient("Error validation Pass Type IDs")
	}

	return s, err
}

// CreatePassTypeID - add new Pass Type ID
func (client *AppleClient) CreatePassTypeID(name, id string) (*CreatePassTypeIDAPIResponse, error) {
	client.printDebug("Creating new Pass Type ID")
	body := strings.NewReader(`teamId=` + client.Config.TeamID + `&name=` + name + `&identifier=` + id)
	req, err := http.NewRequest("POST", createPassTypeIDURL, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("X-Requested-With", "XMLHttpRequest")
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Csrf", client.Csrf)
	req.Header.Set("Csrf_ts", client.CsrfTs)

	resp, err := client.HTTPClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	defer client.ParseCsrf(resp)

	if resp.StatusCode != 200 {
		return nil, NewErrAppleClient("Error creating Pass Type ID")
	}

	// Parse JSON
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var s = new(CreatePassTypeIDAPIResponse)
	err = json.Unmarshal(respBody, &s)

	// Check ResultCode from JSON result
	if s.ResultCode != 0 {
		return s, NewErrAppleClient("Error creating Pass Type ")
	}

	return s, err
}
