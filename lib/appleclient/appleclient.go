// Package appleclient implements Apple developer account console functions.
// There are functions for authentication (in auth.go), certificates (in in cert.go) and pass type IDs (in pass.go).
package appleclient

import (
	"log"
	"net/http"
	"net/http/cookiejar"
	"strconv"
	"time"
)

// Apple URLs
const loginURL = "https://idmsa.apple.com/IDMSWebAuth/authenticate"
const checkPermisions = "https://developer.apple.com/services-account/checkPermissions"
const listCertificateURL = "https://developer.apple.com/services-account/QH65B2/account/ios/certificate/listCertRequests.action?content-type=text/x-url-arguments&accept=application/json&requestId=b01cd081-d718-44a0-y7ec-d8763e2800e6&userLocale=en_US&types=Y3B2F3TYSI&status=4&certificateStatus=0&type=distribution"
const downloadCertificateURL = "https://developer.apple.com/services-account/QH65B2/account/ios/certificate/downloadCertificateContent.action?content-type=text/x-url-arguments&accept=application/json&requestId=72f87d1c-510f-4ca2-y886-8d73f700dcd8&userLocale=en_US&type=Y3B2F3TYSI"
const createCertificateURL = "https://developer.apple.com/services-account/QH65B2/account/ios/certificate/submitCertificateRequest.action?content-type=text/x-url-arguments&accept=application/json&requestId=fb07359d-5d64-4d90-yb99-f41bfab4cd91&userLocale=en_US"
const getCertificateDetailURL = "https://developer.apple.com/services-account/QH65B2/account/ios/certificate/getCertificate.action?content-type=text/x-url-arguments&accept=application/json&requestId=e37f7c3a-83a8-4d74-y3a2-44e6e483feee&userLocale=en_US"
const listPassTypeIDURL = "https://developer.apple.com/services-account/QH65B2/account/ios/identifiers/listPassTypeIds.action?content-type=text/x-url-arguments&accept=application/json&requestId=cf6e95c4-2015-4a94-y215-a3c75782a4a0&userLocale=en_US"
const getPassTypeIDDetailURL = "https://developer.apple.com/services-account/QH65B2/account/ios/certificate/listCertificatesForOwner.action?content-type=text/x-url-arguments&accept=application/json&requestId=663f707b-fda3-441e-y1b4-cb26519bea11&userLocale=en_US"
const validatePassTypeIDURL = "https://developer.apple.com/services-account/QH65B2/account/ios/identifiers/validatePassTypeId.action?content-type=text/x-url-arguments&accept=application/json&requestId=1c6a3441-5a1e-4b82-y48f-e61e8c7d875b&userLocale=en_US"
const createPassTypeIDURL = "https://developer.apple.com/services-account/QH65B2/account/ios/identifiers/addPassTypeId.action?content-type=text/x-url-arguments&accept=application/json&requestId=ead5b4a9-b848-4187-ycd2-616a31da2aeb&userLocale=en_US"

type (
	// Config - Apple client configuration
	Config struct {
		AppIDKey        string
		AppleID         string
		AccountPassword string
		TeamID          string
		PrintDebug      bool
	}

	// AppleClient - Apple Client is basically http client for functions to operate with certificates
	AppleClient struct {
		HTTPClient *http.Client
		Config
		Csrf   string
		CsrfTs string
	}
)

// NewAppleClient - Initialize new AppleClient
func NewAppleClient(config Config) *AppleClient {
	cookieJar, _ := cookiejar.New(nil)
	client := new(AppleClient)
	client.Config = config
	client.HTTPClient = &http.Client{
		Jar: cookieJar,
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
	return client
}

// ParseCsrf - Parse Csrf from http request and store in apple client
func (client *AppleClient) ParseCsrf(resp *http.Response) {
	csrf := resp.Header.Get("Csrf")
	// fmt.Println(">>>>>>>> csrf: ", csrf)
	if len(csrf) > 0 {
		client.Csrf = csrf
	}

	csrfTs := resp.Header.Get("Csrf_ts")
	// fmt.Println(">>>>>>>> csrfTs: ", csrfTs)
	if len(csrfTs) > 0 {
		client.CsrfTs = csrfTs
	}
}

// NewCsrtTs - Generate new csrf timestamp
func NewCsrtTs() string {
	return strconv.FormatInt(time.Now().UTC().Unix(), 10)
}

func (client *AppleClient) printDebug(msg ...interface{}) {
	if client.Config.PrintDebug {
		log.Println(msg)
	}
}
