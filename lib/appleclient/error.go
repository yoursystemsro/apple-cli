package appleclient

// ErrAppleClient - struct for apple client error
type ErrAppleClient struct {
    message string
}

// NewErrAppleClient - Returns new client error
func NewErrAppleClient(message string) *ErrAppleClient {
    return &ErrAppleClient{
        message: message,
    }
}

// ErrAppleClient error method
func (e *ErrAppleClient) Error() string {
    return e.message
}