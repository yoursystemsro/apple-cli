package appleclient

import (
	"net/http"
	"strings"
)

// Login - Try to authenticate to apple auth IDM and store session
func (client *AppleClient) Login() error {
	client.printDebug("Login to apple developer account")
	config := client.Config
	body := strings.NewReader(`teamId=` + config.TeamID + `&appIdKey=` + config.AppIDKey + `&appleId=` + config.AppleID + `&accountPassword=` + config.AccountPassword)
	req, err := http.NewRequest("POST", loginURL, body)
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := client.HTTPClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	defer client.ParseCsrf(resp)

	// If login success response code must be 302
	if resp.StatusCode != 302 {
		return NewErrAppleClient("Error login to apple developer account")
	}

	return nil
}

// CheckPermissions - Simple request to get CSRF token
func (client *AppleClient) CheckPermissions() error {
	client.printDebug("Checking permissions")
	body := strings.NewReader(`teamId=` + client.Config.TeamID + `&permissions=team.agenttransfer.agreement.get%2Cteam.agenttransfer.cancel%2Cteam.agenttransfer.deny%2Cteam.agenttransfer.initiate%2Cteam.agreement.accept%2Cteam.autorenew.update%2Cteam.member.invite.get%2Cteam.member.invite.revoke%2Cteam.member.invite.send%2Cteam.member.leave%2Cteam.member.remove%2Cteam.member.role.change%2Cteam.server.remove%2Cteam.support.purchase`)
	req, err := http.NewRequest("POST", "https://developer.apple.com/services-account/checkPermissions", body)
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := client.HTTPClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	defer client.ParseCsrf(resp)

	return nil
}
