package appleclient

import (
	"fmt"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"
)

// DumpRequest print request
func DumpRequest(req *http.Request) error {
	fmt.Println("------------- REQ -------------")
	dump, err := httputil.DumpRequest(req, true)
	if err != nil {
		return err
	}
	fmt.Printf("%s", dump)
	fmt.Println("------------- / REQ -------------")
	return nil
}

// DumpResponse print response
func DumpResponse(resp *http.Response) error {
	fmt.Println("------------- RESP -------------")
	dump, err := httputil.DumpResponse(resp, true)
	if err != nil {
		return err
	}
	fmt.Printf("%s", dump)
	fmt.Println("------------- / RESP -------------")
	return nil
}

// DumpRequestHeaders - print response or request headers
func DumpRequestHeaders(req *http.Request) {
	fmt.Println("------------- REQ HEADERS-------------")
	for name, headers := range req.Header {
		// name = strings.ToLower(name)
		for _, h := range headers {
			fmt.Printf("%s: %s\n", name, h)
		}
	}
	fmt.Println("------------- / REQ HEADERS-------------")
}

// DumpResponseHeaders - print response or request headers
func DumpResponseHeaders(resp *http.Response) {
	fmt.Println("------------- RESP HEADERS-------------")
	for name, headers := range resp.Header {
		// name = strings.ToLower(name)
		for _, h := range headers {
			fmt.Printf("%s: %s\n", name, h)
		}
	}
	fmt.Println("------------- / RESP HEADERS-------------")
}

// PrepareSearchParameter - prepares string with query search parameter from map
func PrepareSearchParameter(searchFilter map[string]interface{}) string {
	keys := make([]string, 0, len(searchFilter))
	for k, v := range searchFilter {
		keys = append(keys, k+"="+v.(string))
	}
	return url.QueryEscape(strings.Join(keys, "&"))
}
