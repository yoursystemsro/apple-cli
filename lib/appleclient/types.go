package appleclient

import (
	"time"
)

// CertificateListAPIResponse - API JSON response with list of certificates
type CertificateListAPIResponse struct {
	CreationTimestamp time.Time `json:"creationTimestamp"`
	ResultCode        int       `json:"resultCode"`
	UserLocale        string    `json:"userLocale"`
	ProtocolVersion   string    `json:"protocolVersion"`
	RequestID         string    `json:"requestId"`
	RequestURL        string    `json:"requestUrl"`
	ResponseID        string    `json:"responseId"`
	IsAdmin           bool      `json:"isAdmin"`
	IsMember          bool      `json:"isMember"`
	IsAgent           bool      `json:"isAgent"`
	PageNumber        int       `json:"pageNumber"`
	PageSize          int       `json:"pageSize"`
	TotalRecords      int       `json:"totalRecords"`
	CertRequests      []struct {
		CertRequestID            string    `json:"certRequestId"`
		Name                     string    `json:"name"`
		StatusCode               int       `json:"statusCode"`
		StatusString             string    `json:"statusString"`
		CsrPlatform              string    `json:"csrPlatform"`
		DateRequestedString      string    `json:"dateRequestedString"`
		DateRequested            time.Time `json:"dateRequested"`
		DateCreated              time.Time `json:"dateCreated"`
		ExpirationDate           time.Time `json:"expirationDate"`
		ExpirationDateString     string    `json:"expirationDateString"`
		OwnerType                string    `json:"ownerType"`
		OwnerName                string    `json:"ownerName"`
		OwnerID                  string    `json:"ownerId"`
		CanDownload              bool      `json:"canDownload"`
		CanRevoke                bool      `json:"canRevoke"`
		CertificateID            string    `json:"certificateId"`
		CertificateStatusCode    int       `json:"certificateStatusCode"`
		CertRequestStatusCode    int       `json:"certRequestStatusCode"`
		CertificateTypeDisplayID string    `json:"certificateTypeDisplayId"`
		SerialNum                string    `json:"serialNum"`
		TypeString               string    `json:"typeString"`
		CertificateType          struct {
			CertificateTypeDisplayID string `json:"certificateTypeDisplayId"`
			Name                     string `json:"name"`
			Platform                 string `json:"platform"`
			PermissionType           string `json:"permissionType"`
			DistributionType         string `json:"distributionType"`
			DistributionMethod       string `json:"distributionMethod"`
			OwnerType                string `json:"ownerType"`
			DaysOverlap              int    `json:"daysOverlap"`
			MaxActive                int    `json:"maxActive"`
		} `json:"certificateType"`
		Certificate struct {
			Name                 string      `json:"name"`
			DisplayName          interface{} `json:"displayName"`
			CertificateID        string      `json:"certificateId"`
			SerialNumber         string      `json:"serialNumber"`
			Status               string      `json:"status"`
			StatusCode           int         `json:"statusCode"`
			ExpirationDate       string      `json:"expirationDate"`
			ExpirationDateString string      `json:"expirationDateString"`
			CertificatePlatform  string      `json:"certificatePlatform"`
			CertificateType      struct {
				CertificateTypeDisplayID string `json:"certificateTypeDisplayId"`
				Name                     string `json:"name"`
				Platform                 string `json:"platform"`
				PermissionType           string `json:"permissionType"`
				DistributionType         string `json:"distributionType"`
				DistributionMethod       string `json:"distributionMethod"`
				OwnerType                string `json:"ownerType"`
				DaysOverlap              int    `json:"daysOverlap"`
				MaxActive                int    `json:"maxActive"`
			} `json:"certificateType"`
			HasAskKey bool `json:"hasAskKey"`
		} `json:"certificate"`
	} `json:"certRequests"`
}

// CertificateDetailAPIResponse - API JSON response with certificate detail
type CertificateDetailAPIResponse struct {
	CreationTimestamp time.Time `json:"creationTimestamp"`
	ResultCode        int       `json:"resultCode"`
	UserLocale        string    `json:"userLocale"`
	ProtocolVersion   string    `json:"protocolVersion"`
	RequestID         string    `json:"requestId"`
	RequestURL        string    `json:"requestUrl"`
	ResponseID        string    `json:"responseId"`
	IsAdmin           bool      `json:"isAdmin"`
	IsMember          bool      `json:"isMember"`
	IsAgent           bool      `json:"isAgent"`
	Certificate       struct {
		Name                 string      `json:"name"`
		DisplayName          interface{} `json:"displayName"`
		CertificateID        string      `json:"certificateId"`
		SerialNumber         string      `json:"serialNumber"`
		Status               string      `json:"status"`
		StatusCode           int         `json:"statusCode"`
		ExpirationDate       string      `json:"expirationDate"`
		ExpirationDateString string      `json:"expirationDateString"`
		CertificatePlatform  string      `json:"certificatePlatform"`
		CertificateType      struct {
			CertificateTypeDisplayID string `json:"certificateTypeDisplayId"`
			Name                     string `json:"name"`
			Platform                 string `json:"platform"`
			PermissionType           string `json:"permissionType"`
			DistributionType         string `json:"distributionType"`
			DistributionMethod       string `json:"distributionMethod"`
			OwnerType                string `json:"ownerType"`
			DaysOverlap              int    `json:"daysOverlap"`
			MaxActive                int    `json:"maxActive"`
		} `json:"certificateType"`
		RequesterNickName  string `json:"requesterNickName"`
		RequesterFirstName string `json:"requesterFirstName"`
		RequesterLastName  string `json:"requesterLastName"`
		RequesterEmail     string `json:"requesterEmail"`
		HasAskKey          bool   `json:"hasAskKey"`
	} `json:"certificate"`
}

// PassTypeIDListAPIResponse - API JSON response with list of pass types
type PassTypeIDListAPIResponse struct {
	CreationTimestamp time.Time `json:"creationTimestamp"`
	ResultCode        int       `json:"resultCode"`
	UserLocale        string    `json:"userLocale"`
	ProtocolVersion   string    `json:"protocolVersion"`
	RequestID         string    `json:"requestId"`
	RequestURL        string    `json:"requestUrl"`
	ResponseID        string    `json:"responseId"`
	IsAdmin           bool      `json:"isAdmin"`
	IsMember          bool      `json:"isMember"`
	IsAgent           bool      `json:"isAgent"`
	PageNumber        int       `json:"pageNumber"`
	PageSize          int       `json:"pageSize"`
	TotalRecords      int       `json:"totalRecords"`
	PassTypeIDList    []struct {
		Name       string `json:"name"`
		Prefix     string `json:"prefix"`
		Identifier string `json:"identifier"`
		Status     string `json:"status"`
		CanEdit    bool   `json:"canEdit"`
		CanDelete  bool   `json:"canDelete"`
		ShoeboxID  string `json:"shoeboxId"`
		PassTypeID string `json:"passTypeId"`
	} `json:"passTypeIdList"`
	ShoeboxIDList []struct {
		Name       string `json:"name"`
		Prefix     string `json:"prefix"`
		Identifier string `json:"identifier"`
		Status     string `json:"status"`
		CanEdit    bool   `json:"canEdit"`
		CanDelete  bool   `json:"canDelete"`
		ShoeboxID  string `json:"shoeboxId"`
		PassTypeID string `json:"passTypeId"`
	} `json:"shoeboxIdList"`
}

// PassTypeIDDetailPIResponse - API JSON response with Pass Type ID detail
type PassTypeIDDetailPIResponse struct {
	CreationTimestamp time.Time `json:"creationTimestamp"`
	ResultCode        int       `json:"resultCode"`
	UserLocale        string    `json:"userLocale"`
	ProtocolVersion   string    `json:"protocolVersion"`
	RequestID         string    `json:"requestId"`
	RequestURL        string    `json:"requestUrl"`
	ResponseID        string    `json:"responseId"`
	IsAdmin           bool      `json:"isAdmin"`
	IsMember          bool      `json:"isMember"`
	IsAgent           bool      `json:"isAgent"`
	Certificates      []struct {
		Name                 string `json:"name"`
		DisplayName          string `json:"displayName"`
		CertificateID        string `json:"certificateId"`
		SerialNumber         string `json:"serialNumber"`
		Status               string `json:"status"`
		StatusCode           int    `json:"statusCode"`
		ExpirationDate       string `json:"expirationDate"`
		ExpirationDateString string `json:"expirationDateString"`
		CertificatePlatform  string `json:"certificatePlatform"`
		CertificateType      struct {
			CertificateTypeDisplayID string `json:"certificateTypeDisplayId"`
			Name                     string `json:"name"`
			Platform                 string `json:"platform"`
			PermissionType           string `json:"permissionType"`
			DistributionType         string `json:"distributionType"`
			DistributionMethod       string `json:"distributionMethod"`
			OwnerType                string `json:"ownerType"`
			DaysOverlap              int    `json:"daysOverlap"`
			MaxActive                int    `json:"maxActive"`
		} `json:"certificateType"`
		CanDownload bool `json:"canDownload"`
		CanRevoke   bool `json:"canRevoke"`
		HasAskKey   bool `json:"hasAskKey"`
	} `json:"certificates"`
}

// ValidatePassTypeIDAPIResponse - API JSON response with pass type ID validation
type ValidatePassTypeIDAPIResponse struct {
	CreationTimestamp time.Time `json:"creationTimestamp"`
	ResultCode        int       `json:"resultCode"`
	UserLocale        string    `json:"userLocale"`
	ProtocolVersion   string    `json:"protocolVersion"`
	RequestID         string    `json:"requestId"`
	RequestURL        string    `json:"requestUrl"`
	ResponseID        string    `json:"responseId"`
	IsAdmin           bool      `json:"isAdmin"`
	IsMember          bool      `json:"isMember"`
	IsAgent           bool      `json:"isAgent"`
}

// CreatePassTypeIDAPIResponse - API JSON response with result after create new Pass type ID
type CreatePassTypeIDAPIResponse struct {
	CreationTimestamp time.Time `json:"creationTimestamp"`
	ResultCode        int       `json:"resultCode"`
	UserLocale        string    `json:"userLocale"`
	ProtocolVersion   string    `json:"protocolVersion"`
	RequestID         string    `json:"requestId"`
	RequestURL        string    `json:"requestUrl"`
	ResponseID        string    `json:"responseId"`
	IsAdmin           bool      `json:"isAdmin"`
	IsMember          bool      `json:"isMember"`
	IsAgent           bool      `json:"isAgent"`
	PassTypeID        struct {
		Name       string `json:"name"`
		Prefix     string `json:"prefix"`
		Identifier string `json:"identifier"`
		Status     string `json:"status"`
		ShoeboxID  string `json:"shoeboxId"`
		PassTypeID string `json:"passTypeId"`
	} `json:"passTypeId"`
	ShoeboxID struct {
		Name       string `json:"name"`
		Prefix     string `json:"prefix"`
		Identifier string `json:"identifier"`
		Status     string `json:"status"`
		ShoeboxID  string `json:"shoeboxId"`
		PassTypeID string `json:"passTypeId"`
	} `json:"shoeboxId"`
}

// CreateCertificateAPIResponse - API JSON response with create certificate result
type CreateCertificateAPIResponse struct {
	CreationTimestamp time.Time `json:"creationTimestamp"`
	ResultCode        int       `json:"resultCode"`
	UserLocale        string    `json:"userLocale"`
	ProtocolVersion   string    `json:"protocolVersion"`
	RequestID         string    `json:"requestId"`
	RequestURL        string    `json:"requestUrl"`
	ResponseID        string    `json:"responseId"`
	IsAdmin           bool      `json:"isAdmin"`
	IsMember          bool      `json:"isMember"`
	IsAgent           bool      `json:"isAgent"`
	CertRequest       struct {
		CertRequestID            string    `json:"certRequestId"`
		Name                     string    `json:"name"`
		StatusCode               int       `json:"statusCode"`
		StatusString             string    `json:"statusString"`
		CsrPlatform              string    `json:"csrPlatform"`
		DateRequestedString      string    `json:"dateRequestedString"`
		DateRequested            time.Time `json:"dateRequested"`
		DateCreated              time.Time `json:"dateCreated"`
		ExpirationDate           time.Time `json:"expirationDate"`
		ExpirationDateString     string    `json:"expirationDateString"`
		OwnerType                string    `json:"ownerType"`
		OwnerName                string    `json:"ownerName"`
		OwnerID                  string    `json:"ownerId"`
		CertificateID            string    `json:"certificateId"`
		CertificateStatusCode    int       `json:"certificateStatusCode"`
		CertRequestStatusCode    int       `json:"certRequestStatusCode"`
		CertificateTypeDisplayID string    `json:"certificateTypeDisplayId"`
		SerialNum                string    `json:"serialNum"`
		TypeString               string    `json:"typeString"`
		CertificateType          struct {
			CertificateTypeDisplayID string `json:"certificateTypeDisplayId"`
			Name                     string `json:"name"`
			Platform                 string `json:"platform"`
			PermissionType           string `json:"permissionType"`
			DistributionType         string `json:"distributionType"`
			DistributionMethod       string `json:"distributionMethod"`
			OwnerType                string `json:"ownerType"`
			DaysOverlap              int    `json:"daysOverlap"`
			MaxActive                int    `json:"maxActive"`
		} `json:"certificateType"`
		Certificate struct {
			Name                 string `json:"name"`
			DisplayName          string `json:"displayName"`
			CertificateID        string `json:"certificateId"`
			SerialNumber         string `json:"serialNumber"`
			Status               string `json:"status"`
			StatusCode           int    `json:"statusCode"`
			ExpirationDate       string `json:"expirationDate"`
			ExpirationDateString string `json:"expirationDateString"`
			CertificatePlatform  string `json:"certificatePlatform"`
			CertificateType      struct {
				CertificateTypeDisplayID string `json:"certificateTypeDisplayId"`
				Name                     string `json:"name"`
				Platform                 string `json:"platform"`
				PermissionType           string `json:"permissionType"`
				DistributionType         string `json:"distributionType"`
				DistributionMethod       string `json:"distributionMethod"`
				OwnerType                string `json:"ownerType"`
				DaysOverlap              int    `json:"daysOverlap"`
				MaxActive                int    `json:"maxActive"`
			} `json:"certificateType"`
			HasAskKey bool `json:"hasAskKey"`
		} `json:"certificate"`
	} `json:"certRequest"`
}
