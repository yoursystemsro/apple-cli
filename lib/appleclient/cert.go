package appleclient

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

// ListCertificates - return list of certificates
func (client *AppleClient) ListCertificates() (*CertificateListAPIResponse, error) {
	client.printDebug("Listing certificates")
	body := strings.NewReader(`teamId=` + client.Config.TeamID + `&search=&nd=1518944038532&pageSize=1000&pageNumber=1&sidx=name&sort=name%253dasc`)
	req, err := http.NewRequest("POST", listCertificateURL, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := client.HTTPClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	defer client.ParseCsrf(resp)

	if resp.StatusCode != 200 {
		return nil, NewErrAppleClient("Error getting list of certificates")
	}

	// Parse JSON
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var s = new(CertificateListAPIResponse)
	err = json.Unmarshal(respBody, &s)

	// Check ResultCode from JSON result
	if s.ResultCode != 0 {
		return s, NewErrAppleClient("Error getting list of certificates")
	}

	return s, err
}

// GetCertificateDetail - return certificate detail
func (client *AppleClient) GetCertificateDetail(certificateID string) (*CertificateDetailAPIResponse, error) {
	client.printDebug("Getting certificate detail")
	body := strings.NewReader(`certificateId=` + certificateID + `&type=Y3B2F3TYSI`)
	req, err := http.NewRequest("POST", getCertificateDetailURL+"&teamId="+client.Config.TeamID, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("X-Requested-With", "XMLHttpRequest")
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Csrf", client.Csrf)
	req.Header.Set("Csrf_ts", client.CsrfTs)

	resp, err := client.HTTPClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	defer client.ParseCsrf(resp)

	// Check HTTP response code
	if resp.StatusCode != 200 {
		return nil, NewErrAppleClient("Error getting certificate detail")
	}

	// Parse JSON
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var s = new(CertificateDetailAPIResponse)
	err = json.Unmarshal(respBody, &s)

	// Check ResultCode from JSON result
	if s.ResultCode != 0 {
		return s, NewErrAppleClient("Error getting certificate details")
	}

	return s, err
}

// DownloadCertificate - download certificate
func (client *AppleClient) DownloadCertificate(certificateID string) ([]byte, error) {
	client.printDebug("Download certificate ", certificateID)
	getURL := downloadCertificateURL + `&teamId=` + client.Config.TeamID
	req, err := http.NewRequest("GET", getURL, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	q := req.URL.Query()
	q.Add("certificateId", certificateID)
	req.URL.RawQuery = q.Encode()

	resp, err := client.HTTPClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return nil, NewErrAppleClient("Error download certificate")
	}

	respBody, _ := ioutil.ReadAll(resp.Body)
	return respBody, nil
}

// CreateCertificate - create new certifcate for given Pass Type ID
func (client *AppleClient) CreateCertificate(passTypeID string, csr []byte) (*CreateCertificateAPIResponse, error) {
	client.printDebug("Creating certificate")
	body := strings.NewReader(`teamId=` + client.Config.TeamID + `&type=Y3B2F3TYSI&csrContent=` + url.QueryEscape(string(csr)) + `&passTypeId=` + passTypeID + `&specialIdentifierDisplayId=` + passTypeID)
	req, err := http.NewRequest("POST", createCertificateURL, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("X-Requested-With", "XMLHttpRequest")
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Csrf", client.Csrf)
	req.Header.Set("Csrf_ts", client.CsrfTs)

	resp, err := client.HTTPClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	// Parse JSON
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var s = new(CreateCertificateAPIResponse)
	err = json.Unmarshal(respBody, &s)

	// Check ResultCode from JSON result
	if s.ResultCode != 0 {
		return s, NewErrAppleClient("Error creating certificate for Pass type ID " + passTypeID)
	}

	return s, err
}
